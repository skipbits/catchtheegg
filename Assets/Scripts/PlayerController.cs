﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float speed = 10f;
    public GameObject eggPrefab;
    public GameObject scoreObj;


    private int eggCaught = 0;
	// Use this for initialization
	void Start () {
        eggPrefab.rigidbody2D.gravityScale = 0.4f;
        InvokeRepeating("GenerateEgg", 1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {


        //Control The Basket
        Vector2 newPos;

        if (Mathf.Abs(transform.position.x) <= 6.3f)
        {
            newPos = new Vector2(1, 0) * Input.GetAxis("Horizontal") * Time.deltaTime * speed;
            transform.Translate(newPos);
        }
        else
        {

            transform.position = new Vector2(Mathf.Sign(transform.position.x) * 6.3f, transform.position.y);
        }

        //print(Time.timeSinceLevelLoad);
	}

    void GenerateEgg()
    {
        if (Mathf.RoundToInt(Time.timeSinceLevelLoad % 15) == 0)
        {
            eggPrefab.GetComponent<Rigidbody2D>().gravityScale += 0.07f;
            print("Gravity is now: " + eggPrefab.GetComponent<Rigidbody2D>().gravityScale);
        }
            
        Instantiate(eggPrefab,new Vector2(Random.Range(-3,3),6),Quaternion.identity);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        eggCaught++;
        Destroy(col.gameObject);
        scoreObj.GetComponent<UILabel>().text = "Score: " + eggCaught;


        //Setting High Score
        if(PlayerPrefs.HasKey("highScore")){
            PlayerPrefs.SetInt("lastScore", eggCaught);

            if (eggCaught > PlayerPrefs.GetInt("highScore"))
                PlayerPrefs.SetInt("highScore", eggCaught);
        }
        else
        {
            PlayerPrefs.SetInt("lastScore", eggCaught);
            PlayerPrefs.SetInt("highScore", eggCaught);
        }
    }
}
