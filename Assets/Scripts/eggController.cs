﻿using UnityEngine;
using System.Collections;

public class eggController : MonoBehaviour {

    public GameObject Lives;

	// Use this for initialization
	void Start () {
        Lives = GameObject.Find("Lives") as GameObject;
	}
	
	// Update is called once per frame
	void Update () {

        if (transform.position.y < -3.41f)
            gameObject.renderer.material.color = Color.red;

    
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        //print(col.gameObject.name);
        if(col.gameObject.name=="Bar")
        {
            //print("Ouch!");
            TakeDamage();
            Destroy(this.gameObject);
        }
    }
    /// <summary>
    /// Subtracts a life, if life is 0, game is over
    /// </summary>
    void TakeDamage()
    {
        if(Lives.transform.childCount>0)
            Destroy(Lives.transform.GetChild(0).gameObject);
        else
        {
            
            //Time.timeScale = 0;
            //Lets store high score in realtime.
            Application.LoadLevel("EndScene");
        }
    }
}
