﻿using UnityEngine;
using System.Collections;

public class endScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (PlayerPrefs.HasKey("highScore"))
            GetComponent<UILabel>().text = PlayerPrefs.GetInt("highScore").ToString();
        else
            GetComponent<UILabel>().text = "Did you just score ZERO?";

        if(GameObject.Find("lastscore")!=null)
        GameObject.Find("lastscore").GetComponent<UILabel>().text = PlayerPrefs.GetInt("lastScore").ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
